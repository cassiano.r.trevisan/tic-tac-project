package uk.co.wowcher.repo;

import java.util.HashMap;
import java.util.Map;

/**
 * Repository to keep tens.
 */
public final class TensRepo {

    private static final Map<String, String> tens = new HashMap<>();

    static  {
        tens.put("2","twenty");
        tens.put("3","thirty");
        tens.put("4","forty");
        tens.put("5","fifty");
        tens.put("6","sixty");
    }

    public static String getTens(String key){
        return tens.get(key);
    }
}
