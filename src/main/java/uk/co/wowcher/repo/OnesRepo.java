package uk.co.wowcher.repo;

import java.util.HashMap;
import java.util.Map;

/**
 * Repository to keep the ones.
 */
public final class OnesRepo {

    private static final Map<String, String> ones = new HashMap<>();

    static {
        ones.put("0", "");
        ones.put("1", "one");
        ones.put("2", "two");
        ones.put("3", "three");
        ones.put("4", "four");
        ones.put("5", "five");
        ones.put("6", "six");
        ones.put("7", "seven");
        ones.put("8", "eight");
        ones.put("9", "nine");
        ones.put("10", "ten");
        ones.put("11", "eleven");
        ones.put("12", "twelve");
        ones.put("13", "thirteen");
        ones.put("14", "fourteen");
        ones.put("15", "fifteen");
        ones.put("16", "sixteen");
        ones.put("17", "seventeen");
        ones.put("18", "eighteen");
        ones.put("19", "nineteen");
    }

    public static String getOnes(String key){
        return ones.get(key);
    }
}
