package uk.co.wowcher;

import uk.co.wowcher.service.TimeAsWordsService;

import java.time.LocalTime;

public class Main {

    public static void main(String[] args) {

        // Using java 8 api here to get time from the system.
        LocalTime localTime = LocalTime.now();
        int hour = localTime.getHour();
        int minutes = localTime.getMinute();

        System.out.println(
                TimeAsWordsService.getGreeting(hour, minutes) +
                        TimeAsWordsService.getTimeAsWord(hour, minutes));
    }
}
