package uk.co.wowcher.service;

import uk.co.wowcher.repo.OnesRepo;
import uk.co.wowcher.repo.TensRepo;

/**
 * I design this service class to be a method factory.
 * Thus why all the static methods.
 */
public final class TimeAsWordsService {

    private static final String MIDNIGHT = "Midnight";
    private static final String MIDDAY = "Midday";
    private static final String GOOD_EVENING_IT_S = "Good Evening, It's ";
    private static final String GOOD_AFTERNOON_IT_S = "Good Afternoon, It's ";
    private static final String GOOD_NIGHT_IT_S = "Good Night, It's ";
    private static final String GOOD_MORNING_IT_S = "Good Morning, It's ";
    private static final String HOUR_OUTSIDE_LIMITS = "Hour outside limits";
    private static final String MINUTE_OUTSIDE_LIMITS = "Minute outside limits";


    private static final String convertNumberToWord(int number){

        String numberStr = String.valueOf(number);
        String firstNumberHour = String.valueOf(numberStr.charAt(0));
        String secondNumberHour = "";
        //from 10 to 19, we need to split the number
        if(numberStr.length() > 1){
            secondNumberHour = String.valueOf(numberStr.charAt(1));
        }
        //this boolean allows us to know when to go after
        //ones or tens and ones
        boolean isLessThanTwenty = number < 20;

        if(isLessThanTwenty){
            return(OnesRepo.getOnes(numberStr));
        }
        else{
            return(TensRepo.getTens(firstNumberHour) +" "+ OnesRepo.getOnes(secondNumberHour)) ;
        }
    }

    public static final String getTimeAsWord(int hour, int minute){
        //As part of a defensive design we want to make sure these numbers
        //are suitable for a clock.
        try {
            validateInput(hour, minute);
        }catch(RuntimeException rte){
            throw rte;
        }
        if(hour == 0 && minute == 0){
            return MIDNIGHT;
        }
        else if(hour == 12 && minute == 0){
            return MIDDAY;
        }
        else if(hour == 0 && minute != 0){
            return MIDNIGHT + " and " + convertNumberToWord(minute);
        }
        return convertNumberToWord(hour) + " " + convertNumberToWord(minute);
    }

    private static void validateInput(int hour, int minute) throws RuntimeException{
        if(hour < 0 || hour > 23){
            throw new RuntimeException(HOUR_OUTSIDE_LIMITS);
        }
        else if(minute < 0 || minute > 59){
            throw new RuntimeException(MINUTE_OUTSIDE_LIMITS);
        }
    }

    public static final String getGreeting(int hour, int minute){
        if(hour < 12){
            return GOOD_MORNING_IT_S;
        }
        else if(hour >= 12 && hour < 18){
            return GOOD_AFTERNOON_IT_S;
        }
        else if(hour >= 18 && hour < 23){
            return GOOD_EVENING_IT_S;
        }
        else if(hour == 23 && minute <= 30){
            return GOOD_EVENING_IT_S;
        }
        else{
            return GOOD_NIGHT_IT_S;
        }
    }
}
