package uk.co.wowcher.service;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 * Test class. I got 100% coverage according to intelliJ.
 * Also payed attention on edge cases.
 */
public final class TimeAsWordsServiceTest {

    private static final String MIDDAY = "Midday";
    private static final String MID_NIGHT = "Midnight";
    private static final String MID_NIGHT_THIRTY = "Midnight and thirty ";
    private static final String MIDDAY_THIRTY = "twelve thirty ";
    private static final String EIGHT_THIRTY_FOUR = "eight thirty four";
    private static final String FOUR_FIFTY_NINE = "four fifty nine";
    private static final String GOOD_EVENING_IT_S = "Good Evening, It's ";
    private static final String GOOD_AFTERNOON_IT_S = "Good Afternoon, It's ";
    private static final String GOOD_NIGHT_IT_S = "Good Night, It's ";
    private static final String GOOD_MORNING_IT_S = "Good Morning, It's ";

    /**
     * Defensive programming
     */
    @Test(expected = RuntimeException.class)
    public void hugeHourTest(){
        String timeResult = TimeAsWordsService.getTimeAsWord(1200,1400);
    }

    @Test(expected = RuntimeException.class)
    public void hugeMinuteTest(){
        String timeResult = TimeAsWordsService.getTimeAsWord(1200,1400);
    }

    @Test(expected = RuntimeException.class)
    public void negativeHourTest(){
        String timeResult = TimeAsWordsService.getTimeAsWord(-5,1400);
    }

    @Test(expected = RuntimeException.class)
    public void negativeMinuteTest(){
        String timeResult = TimeAsWordsService.getTimeAsWord(14,-500);
    }

    @Test
    public void middayTest(){
        String timeResult = TimeAsWordsService.getTimeAsWord(12,0);
        assertThat(MIDDAY, equalTo(timeResult));
    }

    @Test
    public void midnightTest(){
        String timeResult = TimeAsWordsService.getTimeAsWord(0,0);
        assertThat(MID_NIGHT, equalTo(timeResult));
    }

    @Test
    public void midnightThirtyTest(){
        String timeResult = TimeAsWordsService.getTimeAsWord(0,30);
        assertThat(MID_NIGHT_THIRTY, equalTo(timeResult));
    }

    @Test
    public void middayThirtyTest(){
        String timeResult = TimeAsWordsService.getTimeAsWord(12,30);
        assertThat(MIDDAY_THIRTY, equalTo(timeResult));
    }

    @Test
    public void eightThirtyFourFourTest(){
        String timeResult = TimeAsWordsService.getTimeAsWord(8,34);
        assertThat(EIGHT_THIRTY_FOUR, equalTo(timeResult));
    }

    @Test
    public void fourFiftyNineFourTest(){
        String timeResult = TimeAsWordsService.getTimeAsWord(4,59);
        assertThat(FOUR_FIFTY_NINE, equalTo(timeResult));
    }

    @Test
    public void goodMorningTest(){
        String greetingResult = TimeAsWordsService.getGreeting(4,59);
        assertThat(GOOD_MORNING_IT_S, equalTo(greetingResult));
    }

    @Test
    public void goodAfternoonTest(){
        String greetingResult = TimeAsWordsService.getGreeting(12,00);
        assertThat(GOOD_AFTERNOON_IT_S, equalTo(greetingResult));
    }

    @Test
    public void goodEveningTest(){
        String greetingResult = TimeAsWordsService.getGreeting(19,59);
        assertThat(GOOD_EVENING_IT_S, equalTo(greetingResult));
    }

    @Test
    public void goodEveningEdgeTest(){
        String greetingResult = TimeAsWordsService.getGreeting(23,30);
        assertThat(GOOD_EVENING_IT_S, equalTo(greetingResult));
    }

    @Test
    public void goodEveningEdge2Test(){
        String greetingResult = TimeAsWordsService.getGreeting(23,31);
        assertThat(GOOD_NIGHT_IT_S, equalTo(greetingResult));
    }

    @Test
    public void goodNightTest(){
        String greetingResult = TimeAsWordsService.getGreeting(23,59);
        assertThat(GOOD_NIGHT_IT_S, equalTo(greetingResult));
    }
}
